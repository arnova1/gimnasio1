package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Ficha;

import java.util.Optional;

public interface IFichaService {


    //metodo para leer
    public Iterable<Ficha> read();

    //metodo para crear
    public Ficha create(Ficha ficha);

    //metodo para actualizar
    public Ficha update(Ficha ficha);

    //metodo para borrar
    public void delete(Integer idFicha);

    //busqueda por ID
    public Optional<Ficha> getByIdFicha(Integer idFicha);
}
