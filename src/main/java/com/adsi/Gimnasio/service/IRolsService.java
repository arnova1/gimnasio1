package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Rols;

import java.util.Optional;

public interface IRolsService{


    //metodo para leer
    public Iterable<Rols> read();

    //metodo para crear
    public Rols create(Rols rols);

    //metodo para actualizar
    public Rols update(Rols rols);

    //metodo para borrar
    public void delete(Integer idRols);

    //busqueda por ID
    public Optional<Rols> getByIdRols(Integer idRols);
}
