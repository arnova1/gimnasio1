package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Users;
import com.adsi.Gimnasio.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IUsersServiceImpl implements IUsersService{

    @Autowired
    UsersRepository usersRepository;
//implementacion de los metodos
    @Override
    public Iterable<Users> read() {
        return usersRepository.findAll();
    }

    @Override
    public Users create(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public Users update(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public void delete(Integer idUsers) {
        usersRepository.deleteById(idUsers);
    }

    @Override
    public Optional<Users> getByIdFicha(Integer idUsers) {
        return usersRepository.findById(idUsers);
    }
}
