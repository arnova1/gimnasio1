package com.adsi.Gimnasio.repository;

import com.adsi.Gimnasio.domain.Ficha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FichaRepository extends JpaRepository<Ficha, Integer> {


}
