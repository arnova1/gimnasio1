package com.adsi.Gimnasio.repository;

import com.adsi.Gimnasio.domain.Rols;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolsRepository extends JpaRepository<Rols, Integer> {
}
