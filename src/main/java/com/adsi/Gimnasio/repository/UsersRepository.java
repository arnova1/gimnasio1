package com.adsi.Gimnasio.repository;

import com.adsi.Gimnasio.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Integer> {
}
