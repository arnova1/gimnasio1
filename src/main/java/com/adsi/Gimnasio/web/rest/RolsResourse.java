package com.adsi.Gimnasio.web.rest;

import com.adsi.Gimnasio.domain.Rols;
import com.adsi.Gimnasio.service.IRolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RolsResourse {

    @Autowired
    IRolsService rolsService;

    @GetMapping("/rols")
    public Iterable<Rols> read(){
        return rolsService.read();
    }

    @PostMapping("/rols")
    public Rols create(@RequestBody Rols rols){
        return rolsService.create(rols);
    }

    @PutMapping("/rols")
    public Rols update(@RequestBody Rols rols){
        return rolsService.update(rols);
    }

    @DeleteMapping("/rols/{idRols}")
    public void delete(@PathVariable Integer idRols){
        rolsService.delete(idRols);
    }

    @GetMapping("/rols/{idRols}")
    public Optional<Rols> getById(@PathVariable Integer idRols){
        return rolsService.getByIdRols(idRols);
    }
}
