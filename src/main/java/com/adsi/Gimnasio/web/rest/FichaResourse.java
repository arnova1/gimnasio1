package com.adsi.Gimnasio.web.rest;

import com.adsi.Gimnasio.domain.Ficha;
import com.adsi.Gimnasio.service.IFichaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class FichaResourse {

    @Autowired
    IFichaService fichaService;

    @GetMapping("/ficha")
    public Iterable<Ficha> read(){
        return fichaService.read();
    }

    @PostMapping("/ficha")
    public Ficha create(@RequestBody Ficha ficha){
        return fichaService.create(ficha);
    }

    @PutMapping("/ficha")
    public Ficha update(@RequestBody Ficha ficha){
        return fichaService.update(ficha);
    }

    @DeleteMapping("/ficha/{idFicha}")
    public void delete(@PathVariable Integer idFicha){
        fichaService.delete(idFicha);
    }

    @GetMapping("/ficha/{idFicha}")
    public Optional<Ficha> getByIdFicha(@PathVariable Integer idFicha){
        return fichaService.getByIdFicha(idFicha);
    }
}
