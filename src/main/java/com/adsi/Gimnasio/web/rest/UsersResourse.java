package com.adsi.Gimnasio.web.rest;

import com.adsi.Gimnasio.domain.Users;
import com.adsi.Gimnasio.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UsersResourse {

    @Autowired
    IUsersService usersService;

    @GetMapping("/users")
    public Iterable<Users> read() {
        return usersService.read();
    }

    @PostMapping("/users")
    public Users create(@RequestBody Users users){
        return usersService.create(users);
    }

    @PutMapping("/users")
    public Users update(@RequestBody Users users){
        return usersService.update(users);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer idUsers){
        usersService.delete(idUsers);
    }

    @GetMapping("/users/{id}")
    public Optional<Users> getById(@PathVariable Integer idUsers){
        return usersService.getByIdFicha(idUsers);
    }
}
